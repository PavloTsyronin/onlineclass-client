export interface Work{
    id: number;
    content: string;
    studentEmail: string;
    homeTaskId: number;
    rate: number;
    submitTime: Date;
}

export interface SubmitWork{
    content: string;
    studentEmail: string;
    homeTaskId: number;
}