export interface Task{
    id: number;
    text: string;
    deadLine: Date;
}

export interface Forecast{
    date: Date;
    temperatureC: number;
    temperatureF: number;
    summary: string;
}