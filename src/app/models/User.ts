export interface User{
    id: number;
    email: string;
    isTeacher: boolean; 
}