export interface AuthModel{
    email: string;
    password: string;
}

export interface AuthResult{
    access_token: string;
    email: string;
    roles: string[]
}