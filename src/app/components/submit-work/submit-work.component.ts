import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SubmitWork, Work } from 'src/app/models/Work';
import { AuthService } from 'src/app/shared/auth.service';
import { WorkService } from 'src/app/shared/work.service';

@Component({
  selector: 'app-submit-work',
  templateUrl: './submit-work.component.html',
  styleUrls: ['./submit-work.component.css']
})
export class SubmitWorkComponent implements OnInit {

  workForm: FormGroup;
  workError: string;
  taskId: number
  constructor(private workService: WorkService,
     private route: ActivatedRoute,
     private authService: AuthService,
     private router: Router
  ) { }

  ngOnInit(): void {
    this.taskId = Number(this.route.snapshot.paramMap.get('id'));
    this.workError = '';
    this.workForm = new FormGroup({
      text: new FormControl(''),
    });
  }

  submitWork(){
    let content = this.workForm.value.text;
    let homeTaskId = this.taskId;
    let studentEmail = this.authService.getCurrentUserValue().email;
    let workModel: SubmitWork = { content: content, homeTaskId: homeTaskId, studentEmail: studentEmail}
    this.workService.submit(workModel).subscribe(
      (x) => {
        alert("work successfully submitted.");
        this.router.navigate(['/tasks']);     
      },
      (exc) => {
        this.workError = exc.error.errorText;
      }
    );
  }
}
