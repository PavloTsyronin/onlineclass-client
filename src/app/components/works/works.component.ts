import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Task } from 'src/app/models/Task';
import { Work } from 'src/app/models/Work';
import { TaskService } from 'src/app/shared/task.service';
import { WorkService } from 'src/app/shared/work.service';

@Component({
  selector: 'app-works',
  templateUrl: './works.component.html',
  styleUrls: ['./works.component.css']
})
export class WorksComponent implements OnInit {

  private taskId: number;
  task: Task;
  taskWorks: Work[];
  loading: boolean = true;
  constructor(private route: ActivatedRoute,
              private workService: WorkService,
              private taskService: TaskService
  ) { }

  ngOnInit(): void {
    this.taskId = Number(this.route.snapshot.paramMap.get('id'));
    this.workService.getTaskWorks(this.taskId).subscribe((works) => {
      this.taskWorks = works;
    });
    this.taskService.GetTaskById(this.taskId).subscribe((task) => {
      this.task = task;
      this.loading = false;
    });
  }
}
