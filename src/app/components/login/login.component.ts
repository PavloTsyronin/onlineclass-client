import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../shared/auth.service';
import { Roles } from 'src/app/constants/Roles';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  loginError: string;
  constructor(private authService: AuthService, private router: Router) {

   }

  ngOnInit() {
    this.loginError = '';
    this.loginForm = new FormGroup({
      email: new FormControl(''),
      password: new FormControl('')
    });
  }

  login(){
    this.authService.login(this.loginForm.value).subscribe(
      (user) => {
        if(user.roles.indexOf(Roles.Teacher) > -1){
            this.router.navigate(['/students']);
        }
        else {
          this.router.navigate(['/tasks']);
        }
      },
      (exc) => {
        this.loginError = exc.error.errorText;
      }
    );
  }


}
