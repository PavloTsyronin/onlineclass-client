import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/shared/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit {

  constructor(private formBuilder: FormBuilder,
              private authService: AuthService,
              private router: Router) { }

  registerForm: FormGroup;
  reigisterError: string;

  ngOnInit() {
    this.registerForm =  new FormGroup({
      email: new FormControl(''),
      password: new FormControl('')
    });
    this.reigisterError = '';
  }


  register(){
    let userRegister = this.registerForm.value;

    this.authService.register(userRegister).subscribe(
      ()=>{
        this.router.navigate(['/login']);
      },
      (exc) =>{
        let foo1 = exc.error.errors;
        let foo3 = exc.error.errors[Object.keys(exc.error.errors)[0]];
        let foo4 = exc.error.errors[Object.keys(exc.error.errors)[0]].errors;
        let foo5 = exc.error.errors[Object.keys(exc.error.errors)[0]].errors[0];
        let foo6 = exc.error.errors[Object.keys(exc.error.errors)[0]].errors[0].errorMessage;


            this.reigisterError = exc.error.errors[Object.keys(exc.error.errors)[0]].errors[0].errorMessage;
      }
    )
  }

}
