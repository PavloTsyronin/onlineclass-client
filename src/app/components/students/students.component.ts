import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/User';
import { UserService } from 'src/app/shared/user.service';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {

  loading: boolean = true;

  constructor(private userService: UserService, private router: Router) { }

  students : User[]

  ngOnInit(): void {
    this.userService.GetUsers().subscribe((users) => {
        this.students = users.filter(u => u.isTeacher === false);
        this.loading = false;
      }
    );
  }

}
