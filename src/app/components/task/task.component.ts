import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IsSubmittedResult } from 'src/app/models/IsSubmittedResult';
import { Task } from 'src/app/models/Task';
import { AuthService } from 'src/app/shared/auth.service';
import { TaskService } from 'src/app/shared/task.service';
import { WorkService } from 'src/app/shared/work.service';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {
  @Input() task: Task;

  isTeacher: boolean;
  submitted: boolean;

  constructor(private authService: AuthService,
              private workService: WorkService,
              private taskService: TaskService,
              private router: Router
  ) { }

  ngOnInit(): void {
    this.isTeacher = this.authService.isTeacher();
    if (!this.isTeacher)
      this.workService.isSubmitted(this.authService.getCurrentUserValue().email, this.task.id).subscribe((res) => {
        let r: IsSubmittedResult = res;
        this.submitted = r.isSubmitted;
      })
  }

  deleteTask(taskId: number): void {
    this.taskService.DeleteTask(taskId).subscribe((x) => {
      window.location.reload();
    })
  }

}
