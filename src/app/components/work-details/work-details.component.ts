import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Task } from 'src/app/models/Task';
import { Work } from 'src/app/models/Work';
import { AuthService } from 'src/app/shared/auth.service';
import { TaskService } from 'src/app/shared/task.service';
import { WorkService } from 'src/app/shared/work.service';

@Component({
  selector: 'app-work-details',
  templateUrl: './work-details.component.html',
  styleUrls: ['./work-details.component.css']
})
export class WorkDetailsComponent implements OnInit {
  workId: number;
  rateForm: FormGroup;
  isTeacher: boolean;
  work: Work;
  task: Task;
  rateError: string;


  constructor(private route: ActivatedRoute,
              private router: Router,
              private workService: WorkService,
              private authService: AuthService,
              private taskService: TaskService
  ) { }

  ngOnInit(): void {
    this.rateError = '';
    this.workId = Number(this.route.snapshot.paramMap.get('id'));
    this.isTeacher = this.authService.isTeacher();
    this.workService.GetWorkById(this.workId).subscribe((work) => {
      this.work = work;
      this.taskService.GetTaskById(work.homeTaskId).subscribe((task) => {
        this.task = task;
      });
    });
    this.rateForm = new FormGroup({
      mark: new FormControl(''),
    });

  }

  rateWork(){
    let mark: number;
    mark = Number(this.rateForm.value.mark);
    this.workService.rateWork(this.workId, mark).subscribe(
      (x) => {
      this.router.navigate(["/students"]);
      },
      (exc) => {
        this.rateError = exc.error.errorText;
      }
    );
  }


}
