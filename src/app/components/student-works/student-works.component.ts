import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Work } from 'src/app/models/Work';
import { WorkService } from 'src/app/shared/work.service';

@Component({
  selector: 'app-student-works',
  templateUrl: './student-works.component.html',
  styleUrls: ['./student-works.component.css']
})
export class StudentWorksComponent implements OnInit {

  studentEmail: string;
  studentWorks: Work[];
  loading: boolean = true;
  constructor(private route: ActivatedRoute, private workService: WorkService) { }

  ngOnInit(): void {
    this.studentEmail = String(this.route.snapshot.paramMap.get('id'));
    this.workService.getStudentWorks(this.studentEmail).subscribe((works) => {
      this.studentWorks = works;
      this.loading = false;
    }) 
  }

}
