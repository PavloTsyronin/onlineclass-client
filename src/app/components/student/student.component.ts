import { Component, Input, OnInit } from '@angular/core';
import { User } from 'src/app/models/User';
import { UserService } from 'src/app/shared/user.service';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {

  @Input() user: User;
  constructor(private userService: UserService) { }

  ngOnInit(): void {
  }

  makeTeacher(email: string){
    this.userService.makeTecher(email).subscribe(() => {
      alert(email + " is now a teacher");
    })
  }

}
