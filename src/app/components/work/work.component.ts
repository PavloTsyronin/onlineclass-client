import { Component, Input, OnInit } from '@angular/core';
import { Work } from 'src/app/models/Work';
import { AuthService } from 'src/app/shared/auth.service';

@Component({
  selector: 'app-work',
  templateUrl: './work.component.html',
  styleUrls: ['./work.component.css']
})
export class WorkComponent implements OnInit {

  @Input() work: Work;

  isTeacher: boolean;

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    this.isTeacher = this.authService.isTeacher();
  }

}
