import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Task } from 'src/app/models/Task';
import { AuthService } from 'src/app/shared/auth.service';
import { TaskService } from 'src/app/shared/task.service';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})

export class TasksComponent implements OnInit {

  taskForm: FormGroup;
  taskError: string;

  isTeacher: boolean
  loading: boolean = true;

  constructor(private taskService: TaskService, private authService: AuthService) { }

  tasks : Task[]

  ngOnInit(): void {
    this.taskService.GetTasks().subscribe((tasks) => {
        this.tasks = tasks;
        this.loading = false;
      }
    );
    this.isTeacher = this.authService.isTeacher();
    this.taskError = '';
    this.taskForm = new FormGroup({
      text: new FormControl(''),
      deadLine: new FormControl(new Date())
    });

  }

  createTask(){
    let newTask = this.taskForm.value;
    this.taskService.CreateTask(newTask).subscribe((x) => {
      this.taskService.GetTasks().subscribe((tasks) => {
        this.tasks = tasks;
      }

    );
    },      
    (exc) => {
      this.taskError = exc.error.errorText;
    })
  }

}
