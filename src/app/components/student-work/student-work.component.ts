import { Component, Input, OnInit } from '@angular/core';
import { Task } from 'src/app/models/Task';
import { Work } from 'src/app/models/Work';
import { AuthService } from 'src/app/shared/auth.service';
import { TaskService } from 'src/app/shared/task.service';

@Component({
  selector: 'app-student-work',
  templateUrl: './student-work.component.html',
  styleUrls: ['./student-work.component.css']
})
export class StudentWorkComponent implements OnInit {

  @Input() work: Work;
  task: Task;

  isTeacher: boolean;

  constructor(private authService: AuthService, private taskService: TaskService) { }

  ngOnInit(): void {
    this.isTeacher = this.authService.isTeacher();
    this.taskService.GetTaskById(this.work.homeTaskId).subscribe((task) => {
      this.task = task;
    })
  }

}
