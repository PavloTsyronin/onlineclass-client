import { Pipe, PipeTransform } from '@angular/core';
import { User } from '../models/User';

@Pipe({
  name: 'isStudent'
})
export class IsStudentPipe implements PipeTransform {

  transform(users: User[], isStudent: boolean): any {
    if (!users || isStudent === undefined){
      return users;
    }

    return users.filter(u => u.isTeacher === !isStudent)
  }

}
