import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { StudentsComponent } from './components/students/students.component';
import { TasksComponent } from './components/tasks/tasks.component';
import { AuthGuard } from './helpers/auth-guard';
import { Roles } from './constants/Roles';
import { SubmitWorkComponent } from './components/submit-work/submit-work.component';
import { WorksComponent } from './components/works/works.component';
import { StudentComponent } from './components/student/student.component';
import { StudentWorksComponent } from './components/student-works/student-works.component';
import { WorkDetailsComponent } from './components/work-details/work-details.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';

const routes: Routes = [
  { path: 'register', component: RegisterComponent },
  { path: 'login', component: LoginComponent },
  { path: 'students', component: StudentsComponent , canActivate: [AuthGuard] , data: {
      roles: [Roles.Teacher]
    }
  },
  { path: 'tasks', component: TasksComponent, canActivate: [AuthGuard] , data: {
    roles: [Roles.Teacher, Roles.User]
  }},
  { path: 'submitWork/:id', component: SubmitWorkComponent, canActivate: [AuthGuard] , data: {
    roles: [Roles.Teacher, Roles.User]
  }},
  { path: 'taskWorks/:id', component: WorksComponent, canActivate: [AuthGuard] , data: {
    roles: [Roles.Teacher]
  }},
  { path: 'studentWorks/:id', component: StudentWorksComponent, canActivate: [AuthGuard] , data: {
    roles: [Roles.Teacher]
  }},
  { path: 'workDetails/:id', component: WorkDetailsComponent, canActivate: [AuthGuard] , data: {
    roles: [Roles.Teacher]
  }},

  { path: '', redirectTo: '/tasks', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }  // Wildcard route for a 404 page
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
