import { Component } from '@angular/core';
import { WeatherServiceService } from '../app/shared/weather-service.service';
import { Forecast } from '../app/models/Task';
import { OnInit } from '@angular/core';
import { AuthService } from './shared/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public title = "OnlineClass";
  public email : string;
  isAuth = false;
  isTeacher: boolean;
 
  constructor(private authService: AuthService, private router: Router){
    this.authService.currentUser.subscribe( user => {
      if(user){
        this.isAuth = true;
        this.email = user.email;
      }
      else{
        this.isAuth = false;
        this.email = '';
      }
    })
 
  }
  ngOnInit(){
   let currentUser =  this.authService.getCurrentUserValue();
   this.isAuth = currentUser == null ? false : true;
   this.email = currentUser == null ? '' : currentUser.email;
   this.isTeacher = this.authService.isTeacher();
  }
  
  logout(){
    this.authService.logout();
    this.router.navigate(['/login']);
  }
 }
 