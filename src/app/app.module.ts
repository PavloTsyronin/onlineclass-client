import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoginComponent } from './components/login/login.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { RegisterComponent } from './components/register/register.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { JwtInterseptor } from './helpers/jwt-interseptor';
import { StudentsComponent } from './components/students/students.component';
import { IsStudentPipe } from './pipes/is-student.pipe';
import { MatListModule } from '@angular/material/list';
import { TasksComponent } from './components/tasks/tasks.component';
import { CommonModule, DatePipe } from '@angular/common';
import { TaskComponent } from './components/task/task.component';
import { StudentComponent } from './components/student/student.component';
import { SubmitWorkComponent } from './components/submit-work/submit-work.component';
import { WorksComponent } from './components/works/works.component';
import { WorkComponent } from './components/work/work.component';
import { StudentWorksComponent } from './components/student-works/student-works.component';
import { StudentWorkComponent } from './components/student-work/student-work.component';
import { WorkDetailsComponent } from './components/work-details/work-details.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    StudentsComponent,
    IsStudentPipe,
    TasksComponent,
    TaskComponent,
    StudentComponent,
    SubmitWorkComponent,
    WorksComponent,
    WorkComponent,
    StudentWorksComponent,
    StudentWorkComponent,
    WorkDetailsComponent,
    PageNotFoundComponent
    ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    FormsModule,
    ReactiveFormsModule,
    MatListModule,
    MatDatepickerModule,
    MatNativeDateModule,
    CommonModule
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS, useClass: JwtInterseptor, multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
