import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../models/User';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  Url:string = 'https://localhost:44388/api/';

  constructor(private http: HttpClient) { }

  GetUsers() : Observable<User[]>{
    return this.http.get<User[]>(this.Url + 'user')
  }

  makeTecher(email: string){
    return this.http.post("https://localhost:44388/maketeacher/" + email, null);
  }
}
