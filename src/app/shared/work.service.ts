import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { IsSubmittedResult } from '../models/IsSubmittedResult';
import { SubmitWork, Work } from '../models/Work';

@Injectable({
  providedIn: 'root'
})
export class WorkService {

  constructor(private http: HttpClient) { }

  private Url:string = 'https://localhost:44388/api/';

  submit(work: SubmitWork){
    return this.http.post(this.Url + 'work', work);
  }

  getTaskWorks(taskId: number): Observable<Work[]>{
    return this.http.get<Work[]>(this.Url + 'work/tasksworks/' + taskId)
  }

  getStudentWorks(studentEmail: string): Observable<Work[]>{
    return this.http.get<Work[]>(this.Url + 'work/studentsworks/' + studentEmail)
  }

  GetWorkById(id: number) : Observable<Work>{
    return this.http.get<Work>(this.Url + 'work/' + id)
  }

  rateWork(workId: number, mark: number){
    return this.http.post(this.Url + 'work/' + workId + '/' + mark, null);
  }

  isSubmitted(email: string, taskId: number){
    return this.http.get<IsSubmittedResult>(this.Url + 'work/' + email + '/' + taskId);
  }


}
