import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AuthModel, AuthResult } from '../models/AuthModel'
import { map, catchError } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private Url:string = 'https://localhost:44388/';

  private currentUserSubject: BehaviorSubject<AuthResult>;
  public currentUser: Observable<AuthResult>;

  constructor(private http: HttpClient) { 
    this.currentUserSubject = new BehaviorSubject<AuthResult>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public getCurrentUserValue(): AuthResult{
    return this.currentUserSubject.value;
  }

  login(authModel: AuthModel){
    return this.http.post(this.Url + 'token', authModel).pipe(
      map((authResult: AuthResult) =>{
        localStorage.setItem('currentUser', JSON.stringify(authResult));
        this.currentUserSubject.next(authResult);
        return authResult;
      }
    ));
  }

  register(userRegister: AuthModel){
    return this.http.post(this.Url + 'register', userRegister);
  }


  logout(){
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
  }

  isTeacher(): boolean{
    return this.getCurrentUserValue().roles.includes("teacher");
  }
}
