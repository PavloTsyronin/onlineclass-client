import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Task } from '../models/Task';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  Url:string = 'https://localhost:44388/api/';

  constructor(private http: HttpClient) { }

  GetTasks() : Observable<Task[]>{
    return this.http.get<Task[]>(this.Url + 'hometask')
  }

  GetTaskById(id: number) : Observable<Task>{
    return this.http.get<Task>(this.Url + 'hometask/' + id)
  }

  DeleteTask(id: number) : Observable<Object> {
    return this.http.delete(this.Url + 'hometask/' + id)
  }

  CreateTask(task: Task){
    return this.http.post(this.Url + 'hometask', task)
  }

}

