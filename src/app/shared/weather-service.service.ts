import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Forecast } from '../models/Task';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
}

@Injectable({
  providedIn: 'root'
})
export class WeatherServiceService {

  forecastUrl:string = 'https://localhost:44388/weatherforecast';
  forecastLimit = '?_limit=5';

  constructor(private http: HttpClient) { }

  getForecasts():Observable<Forecast[]> {
    return this.http.get<Forecast[]>(`${this.forecastUrl}${this.forecastLimit}`);
  }
}
